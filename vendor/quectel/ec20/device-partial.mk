#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifeq ($(BUILD_WITH_EC20),true)

PRODUCT_PACKAGES += \
	rild \
	usb_modeswitch \
	chat

PRODUCT_COPY_FILES += \
	vendor/quectel/ec20/libquectel-ril/armeabi/libreference-ril.so:system/lib/libquectel-ril.so \
	vendor/quectel/ec20/libquectel-ril/armeabi/chat:system/bin/chat \
	vendor/quectel/ec20/libquectel-ril/armeabi/ip-up:system/etc/ppp/ip-up \
	vendor/quectel/ec20/libquectel-ril/armeabi/ip-down:system/etc/ppp/ip-down \
	vendor/quectel/ec20/apns-conf.xml:system/etc/apns-conf.xml

PRODUCT_PROPERTY_OVERRIDES += \
	ro.telephony.default_network=9 \
	rild.libpath=/system/lib/libquectel-ril.so

endif ### BUILD_WITH_EC20 ###
